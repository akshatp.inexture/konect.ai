# ReactJs Folder Structure

- Main branch contains basics setup of react webapp.

## Installation

Download this project's zip or clone it:

```bash
# replace project-dir with directory name.
cd project-dir 
Ex.: >> cd react-folder-structutre

# To install all node/npm packages listed in package.json file.
npm i 
```

## To run the project

In the project directory, you can run:

```bash
npm start
```

## FYI

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) which is default port to view project in your browser.

The page will reload when you make changes and save the code.\
You may also see any lint errors in the console.
