import { useEffect } from "react";
import { useDispatch } from "react-redux";
import Overview from "./pages/Overview";
import { getLeadSource } from "./redux/dashboard/dashboardAction";

function App() {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getLeadSource());
    // eslint-disable-next-line
  }, []);

  return (
    <div>
      <Overview />
    </div>
  );
}

export default App;
