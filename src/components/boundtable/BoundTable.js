import React, { memo, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { getBoundData } from "../../redux/dashboard/dashboardAction";
import "./inbound.scss";

const Inbound = ({ payloadData }) => {
  console.log(payloadData, "payloadData");
  const dispatch = useDispatch();
  const dashboardData = useSelector((state) => state.dashboard);
  console.log(dashboardData.boundData.type, "Bound table");

  useEffect(() => {
    dispatch(
      getBoundData({
        payloadData,
      })
    );
    // eslint-disable-next-line
  }, []);

  const getBoundTableData = (type) => {
    dispatch(
      getBoundData({
        ...payloadData,
        type,
      })
    );
  };
  return (
    <div>
      <nav>
        <div className="nav nav-tabs" id="nav-tab" role="tablist">
          <button
            className="nav-link active"
            id="nav-inbound-tab"
            data-bs-toggle="tab"
            data-bs-target="#nav-inbound"
            type="button"
            role="tab"
            aria-controls="nav-inbound"
            aria-selected="true"
            onClick={() => getBoundTableData("Inbound")}
          >
            Inbound
          </button>
          <button
            className="nav-link"
            id="nav-outbound-tab"
            data-bs-toggle="tab"
            data-bs-target="#nav-outbound"
            type="button"
            role="tab"
            aria-controls="nav-outbound"
            aria-selected="false"
            onClick={() => getBoundTableData("Outbound")}
          >
            Outbound
          </button>
        </div>
      </nav>
      <div className="tab-content" id="nav-tabContent">
        <div
          className="tab-pane fade show active"
          id="nav-inbound"
          role="tabpanel"
          aria-labelledby="nav-inbound-tab"
        >
          <table className="table">
            <thead>
              <tr>
                <th scope="col">Campaign</th>
                <th scope="col">Leads</th>
                <th scope="col">Positive</th>
                <th scope="col">Calls</th>
                <th scope="col">Appts.</th>
              </tr>
            </thead>
            <tbody>
              {dashboardData?.boundData?.data?.map((item, i) => (
                <tr key={i}>
                  <td>{item.campaign_name}</td>
                  <td className="white">{item.leads}</td>
                  <td className="white-blue">{item.positive}</td>
                  <td className="white">{item.calls}</td>
                  <td className="white-blue">{item.appointments}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
        <div
          className="tab-pane fade"
          id="nav-outbound"
          role="tabpanel"
          aria-labelledby="nav-outbound-tab"
        >
          <table className="table">
            <thead>
              <tr>
                <th scope="col">Campaign</th>
                <th scope="col">Leads</th>
                <th scope="col">Positive</th>
                <th scope="col">Calls</th>
                <th scope="col">Appts.</th>
              </tr>
            </thead>
            <tbody>
              {dashboardData?.boundData?.data?.map((item, i) => (
                <tr key={i}>
                  <td>{item.campaign_name}</td>
                  <td className="white">{item.leads}</td>
                  <td className="white-blue">{item.positive}</td>
                  <td className="white">{item.calls}</td>
                  <td className="white-blue">{item.appointments}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
};

export default memo(Inbound);
