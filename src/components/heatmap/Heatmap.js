import React from "react";
import Chart from "react-apexcharts";

const Heatmap = () => {
  const options = {
    chart: {
      type: "heatmap",
      toolbar: {
        show: false,
      },
      events: {
        mounted: function () {
          addYAxisBackground();
          addXAxisBackground();
          addBoxBackground();
        },
        updated: function () {
          addYAxisBackground();
          addXAxisBackground();
          addBoxBackground();
        },
      },
    },
    dataLabels: {
      enabled: false, //display value in box
    },
    colors: ["#008FFB"],
    xaxis: {
      type: "Days",
      categories: ["SUN", "MON", "TUE", "WED", "THUR", "FRI", "SAT"],
      position: "top",
      tooltip: {
        enabled: false,
      },
      labels: {
        style: {
          colors: [
            "#ffffff",
            "#ffffff",
            "#ffffff",
            "#ffffff",
            "#ffffff",
            "#ffffff",
            "#ffffff",
          ],
          fontWeight: "600",
        },
      },
    },
    yaxis: {
      labels: {
        style: {
          fontSize: "12px",
          colors: ["#000000"],
        },
      },
    },
    title: {
      text: "Timetable Heatmap",
      margin: 5,
      style: {
        color: "#155375",
        fontWweight: "700",
        fontSize: "16px",
      },
    },
    stroke: {
      width: 0,
    },
    plotOptions: {
      heatmap: {
        radius: 0,
        // colorScale: {
        //   ranges: [
        //     {
        //       from: 0,
        //       to: 25,
        //       color: "#00A100",
        //       name: "low",
        //     },
        //     {
        //       from: 26,
        //       to: 50,
        //       color: "#128FD9",
        //       name: "medium",
        //     },
        //     {
        //       from: 51,
        //       to: 75,
        //       color: "#FFB200",
        //       name: "high",
        //     },
        //     {
        //       from: 76,
        //       to: 100,
        //       color: "#FF0000",
        //       name: "extreme",
        //     },
        //   ],
        // },
      },
    },
    tooltip: {
      marker: {
        show: true,
      },
    },
  };
  const series = [
    {
      name: "1:00",
      data: [89, 34, 23, 16, 5, 37, 9],
    },
    {
      name: "2:00",
      data: [90, 6, 4, 38, 9, 45, 9],
    },
    {
      name: "3:00",
      data: [89, 3, 56, 12, 78, 2, 89],
    },
    {
      name: "4:00",
      data: [67, 34, 8, 1, 0, 67, 66],
    },
    {
      name: "5:00",
      data: [8, 45, 23, 14, 78, 98, 45],
    },
    {
      name: "6:00",
      data: [2, 3, 56, 78, 90, 12, 90],
    },
    {
      name: "7:00",
      data: [6, 45, 56, 2, 9, 90, 2],
    },
    {
      name: "8:00",
      data: [9, 2, 3, 4, 5, 78, 56],
    },
    {
      name: "9:00",
      data: [1, 45, 78, 45, 90, 7, 6],
    },
    {
      name: "10:00",
      data: [5, 7, 3, 2, 89, 87, 45],
    },
    {
      name: "11:00",
      data: [1, 90, 34, 67, 34, 89, 23],
    },
    {
      name: "12:00",
      data: [0, 6, 7, 34, 25, 78, 89],
    },
    {
      name: "13:00",
      data: [78, 4, 67, 23, 90, 23, 78],
    },
    {
      name: "14:00",
      data: [1, 34, 56, 78, 90, 34, 78],
    },
    {
      name: "15:00",
      data: [90, 78, 56, 34, 2, 6, 9],
    },
    {
      name: "16:00",
      data: [65, 43, 87, 12, 4, 45, 78],
    },
    {
      name: "17:00",
      data: [91, 23, 45, 78, 45, 67, 54],
    },
    {
      name: "18:00",
      data: [55, 33, 2, 8, 98, 56, 78],
    },
    {
      name: "19:00",
      data: [88, 11, 23, 1, 5, 8, 5],
    },
    {
      name: "20:00",
      data: [78, 77, 44, 34, 24, 76, 8],
    },
    {
      name: "21:00",
      data: [89, 45, 34, 23, 78, 67, 56],
    },
    {
      name: "22:00",
      data: [29, 38, 81, 2, 66, 56, 89],
    },
    {
      name: "23:00",
      data: [72, 52, 9, 8, 72, 10, 98],
    },
    {
      name: "24:00",
      data: [10, 20, 30, 40, 50, 60, 70],
    },
  ];

  function addYAxisBackground() {
    var ctx = document.querySelector("svg"),
      textElm = ctx.querySelector("svg g"),
      SVGRect = textElm.getBBox();

    var rect = document.createElementNS("http://www.w3.org/2000/svg", "rect");
    rect.setAttribute("x", SVGRect.x - 33);
    rect.setAttribute("y", SVGRect.y + 86);
    rect.setAttribute("width", "90px");
    rect.setAttribute("height", SVGRect.height - 22);
    rect.setAttribute("fill", "#CFD2CF");
    ctx.insertBefore(rect, textElm);
  }

  function addXAxisBackground() {
    var ctx = document.querySelector("svg"),
      textElm = ctx.querySelector("svg g"),
      SVGRect = textElm.getBBox();

    var rect = document.createElementNS("http://www.w3.org/2000/svg", "rect");
    rect.setAttribute("x", SVGRect.x + 57);
    rect.setAttribute("y", SVGRect.y + 53);
    rect.setAttribute("width", "473px");
    rect.setAttribute("height", SVGRect.height - 450);
    rect.setAttribute("fill", "#2C3333");
    ctx.insertBefore(rect, textElm);
  }

  function addBoxBackground() {
    var ctx = document.querySelector("svg"),
      textElm = ctx.querySelector("svg g"),
      SVGRect = textElm.getBBox();

    var rect = document.createElementNS("http://www.w3.org/2000/svg", "rect");
    rect.setAttribute("x", SVGRect.x - 33);
    rect.setAttribute("y", SVGRect.y + 53);
    rect.setAttribute("width", "90px");
    rect.setAttribute("height", SVGRect.height - 450);
    rect.setAttribute("fill", "#F1EEE9");
    ctx.insertBefore(rect, textElm);
  }

  return (
    <>
      <h6 className="common-col-head">Timetable Heatmap</h6>
      <div className="card">
        <div className="common-bg card-body">
          <Chart
            options={options}
            series={series.reverse()}
            type="heatmap"
            width="550"
            height="550"
          />
        </div>
      </div>
    </>
  );
};

export default Heatmap;
