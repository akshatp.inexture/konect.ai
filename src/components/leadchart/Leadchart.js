import React, { memo } from "react";
import "./Leadchart.scss";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
  Filler,
  plugins,
} from "chart.js";
import { Line } from "react-chartjs-2";
ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
  Filler,
  plugins
);

const Leadchart = () => {
  //   const [labelX, setlabelX] = useState([]);
  //   const [Data, setData] = useState([]);
  const DISPLAY = true;
  const BORDER = true;
  const CHART_AREA = true;
  const TICKS = true;

  //   useEffect(() => {
  //     let arr = [];
  //     graphData.map(
  //       (item) =>
  //         (arr = [
  //           ...arr,
  //           [
  //             moment(item.date).format(" h:mm:ss a"),
  //             moment(item.date).format("MMMM Do YYYY"),
  //           ],
  //         ])
  //     );
  //     setlabelX(arr);
  //   }, [graphData]);

  //   useEffect(() => {
  //     let arr = [];
  //     graphData.map((item) => (arr = [...arr, item.amount]));
  //     setData(arr);
  //   }, [graphData]);

  const data = {
    labels: [
      "Jul 11",
      "Jul 12",
      "Jul 13",
      "Jul 14",
      "Jul 15",
      "Jul 16",
      "Jul 17",
      "Jul 18",
    ],
    datasets: [
      {
        label: "             ",
        data: [0, 50, 100, 150, 200, 250, 300, 500],
        borderColor: "#fda4af",
        borderWidth: 2,
        backgroundColor: "#fda4af",
        // borderDash: [10, 5],
      },
      {
        label: "             ",
        data: [10, 400, 140, 325, 200, 150, 300, 425],
        borderColor: "#a7f3d0",
        borderWidth: 2,
        backgroundColor: "#a7f3d0",
      },
      {
        label: "             ",
        data: [0, 10, 300, 250, 200, 70, 350, 50],
        borderColor: "#67e8f9",
        borderWidth: 2,
        // borderDash: [10, 5],
        backgroundColor: "#67e8f9",
      },
      {
        label: "             ",
        data: [0, 20, 780, 90, 20, 250, 30, 150],
        borderColor: "rgba(255, 0, 0, 1)",
        borderWidth: 2,
        backgroundColor: "rgba(255,0, 0, 1)",
      },
    ],
  };
  const options = {
    responsive: true,
    maintainAspectRatio: true,
    plugins: {
      legend: {
        display: false,
        labels: {
          color: "#000000",
          // This more specific font property overrides the global property
          font: {
            size: 14,
            color: "rgba(255,0, 0, 1)",
          },
          title: {
            size: 14,
            color: "#666",
          },
        },
      },
    },
    scales: {
      x: {
        grid: {
          display: DISPLAY,
          drawBorder: BORDER,
          drawOnChartArea: CHART_AREA,
          drawTicks: TICKS,
          // drawBorder: true,
          color: function (context) {
            if (context.tick.value > 0) {
              return "rgba(255, 255, 255, 1)";
            } else if (context.tick.value < 0) {
              return "rgba(255, 255, 255, 1 )";
            }

            return "#fff";
          },
        },

        //       font: {
        //         size: 15,
        //       },
        //       color: "White",
        //     },
      },
      y: {},
    },
  };

  return (
    <>
      <h6 className="common-col-head">Leads and Engagement</h6>
      <div className="card">
        <div className="common-bg card-body">
          <div className="graph-percentage">
            <div className="p-black">
              <h5>1.4%</h5>
              <div class="change">
                <p>Change</p>
                <span></span>
              </div>
            </div>
            <div className="p-black">
              <h5>1.4K</h5>
              <div class="leads">
                <p>Leads</p>
                <span></span>
              </div>
            </div>
            <div className="p-black">
              <h5>30.4K</h5>
              <div class="engagement">
                <p>Engagement*</p>
                <span></span>
              </div>
            </div>
            <div className="p-grey">
              <h5>1.4K</h5>
              <div class="prev-leads">
                <p>Prev.Leads</p>
                <span></span>
              </div>
            </div>
            <div className="p-grey">
              <h5>30.4K</h5>
              <div class="prev-engagement">
                <p>Prev.Engagement</p>
                <span></span>
              </div>
            </div>
          </div>
          <div>
            <Line options={options} data={data} />
          </div>
          <div class="graph-bottom-text">
            <div class="text-left">
              <label>This Week</label>
              <p>Jul 11 2022 - Jul 14 2022</p>
            </div>
            <div class="text-right">
              <label>*scaled by the division of 100</label>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default memo(Leadchart);
