import React from "react";
// import { TagCloud } from "react-tagcloud";
import TagCloud from "react-tag-cloud";
import "./Wordcloud.scss";

const styles = {
  small: {
    fontSize: 14,
    fontWeight: 700,
    color: "#C2CAD7",
  },
  medium: { fontSize: 20, fontWeight: 700, color: "#8FA5C9" },
  large: {
    fontSize: 32,
    fontWeight: 700,
    color: "#306DCC",
  },
  extraLarge: { fontSize: 64, fontWeight: 700, color: "#F26957" },
};

const Wordcloud = () => {
  // const data = [
  //   { value: "JavaScript", count: 38 },
  //   { value: "React", count: 30 },
  //   { value: "Nodejs", count: 28 },
  //   { value: "Express.js", count: 25 },
  //   { value: "HTML5", count: 33 },
  //   { value: "MongoDB", count: 18 },
  //   { value: "CSS3", count: 20 },
  //   { value: "asdasd", count: 8 },
  //   { value: "asdasdasd", count: 20 },
  // ];
  return (
    <>
      {/* <TagCloud
        style={{
          animation: "blinker 5s linear infinite",
          animationDelay: `${Math.random() * 2}s`,
          margin: "3px",
          padding: "3px",
          display: "inline-block",
          color: "white",
          border: `2px solid red`,
        }}
        randomSeed={22}
        minSize={12}
        maxSize={35}
        tags={data}
        onClick={(tag) => alert(`'${tag.value}' was selected!`)}
      /> */}
      <div className="app-inner">
        <h6 className="common-col-head">Word cloud</h6>
        <TagCloud
          className="tag-cloud"
          style={{
            fontFamily: "sans-serif",
            fontSize: () => Math.round(Math.random() * 50) + 16,
            padding: 5,
          }}
        >
          <div style={styles.medium}>AFTERNOON</div>
          <div style={styles.medium}>1:30 pm</div>
          <div style={styles.medium}>RESCHEDULE</div>
          <div style={styles.large}>APPOINTMENT</div>
          <div style={styles.medium}>TUESDAY</div>
          <div style={styles.larmediumge}>5:30 PM</div>
          <div style={styles.extraLarge}>HONDA CIVIC</div>
          <div style={styles.small}>WINTER TIRES</div>
          <div style={styles.large}>CANCEL</div>
          <div style={styles.small}>PARTS</div>
          <div style={styles.medium}>STOP</div>
          <div style={styles.medium}>2013</div>
          <div style={styles.large}>WINDWIPERS</div>
          <div style={styles.medium}>12:30 PM</div>
          <div style={styles.small}>SCHEDULE</div>
          <div style={styles.medium}>DODGE FORD</div>
          <div style={styles.medium}>COROLLA 2014</div>
          <div style={styles.medium}>3:30 PM</div>
          <div style={styles.medium}>REPRESENTATIVE</div>
          <div style={styles.small}>ALL SEASON TIRES</div>
        </TagCloud>
      </div>
    </>
  );
};

export default Wordcloud;
