import "./modal.scss";
import { Modal } from "react-bootstrap";
import Lightpick from "lightpick";
import moment from "moment";
import { useEffect, useRef, useState } from "react";

const CustomDateModel = ({
  handleClose,
  handleShow,
  setShow,
  show,
  setCustomeDate,
  dateref,
  setDuration,
  setDays,
}) => {
  const from = useRef(null);
  const to = useRef(null);

  const [saveDate, setSaveDate] = useState("");

  useEffect(() => {
    if (show && from.current && to.current) {
      // console.log(from.current, to.current, "modal show");

      var custom_date_picker = new Lightpick({
        field: from.current,
        secondField: to.current,
        // parentEl: ".modal-body",
        singleDate: false,
        numberOfMonths: 2,
        repick: false,
        hideOnBodyClick: false,
        autoClose: false,
        inline: true,
        maxDate: moment(),
        onSelect: function (start, end) {
          var str = "";
          str += start ? start.format("MMM D YYYY") + " - " : "";
          str += end ? end.format("MMM D YYYY") : "";

          setDays(end.diff(start, "days").toString());
          setSaveDate(str);
          console.log(str, "str");
          // (<any>document.getElementById('result-3')).innerHTML = str;
        },
        // eslint-disable-next-line
      });
    }
  }, [show, from, to]);
  const onSaveDate = () => {
    handleClose();
    if (dateref.current) {
      dateref.current.value = saveDate;
      setDuration("Custom");
    }
  };

  return (
    <Modal show={show} onHide={handleClose} className="date-modal">
      {/* <div
            className="modal fade date-modal"
            id="dateBackdrop"
            data-bs-backdrop="date"
            data-bs-keyboard="false"
            tabIndex="-1"
            aria-labelledby="dateBackdropLabel"
            aria-hidden="true"
          > */}
      <div className="modal-dialog modal-dialog-centered">
        <div className="modal-content">
          <Modal.Header closeButton>
            {/* <div className="modal-header"> */}
            <Modal.Title>
              {/* <h5 className="modal-title" id="staticBackdropLabel"> */}
              <h5> CUSTOM TIME PERIOD</h5>
              {/* </h5> */}
            </Modal.Title>

            {/* </div> */}
          </Modal.Header>
          <Modal.Body>
            {/* <div className="modal-body"> */}

            <div className="date-picker-modal">
              <div className="form-group">
                <label>From</label>
                <input type="text" id="from_date" ref={from} />
              </div>
              <div className="form-group">
                <label>To</label>
                <input type="text" id="to_date" ref={to} />
              </div>
            </div>

            {/* </div> */}
          </Modal.Body>
          <Modal.Footer>
            {/* <div className="modal-footer"> */}
            <button
              type="button"
              className="btn btn-secondary cancel-btn"
              data-bs-dismiss="modal"
              onClick={handleClose}
            >
              CANCEL
            </button>
            <button
              type="button"
              className="btn btn-primary save-btn "
              onClick={onSaveDate}
            >
              SAVE
            </button>
            {/* </div> */}
          </Modal.Footer>
        </div>
      </div>
      {/* </div> */}
    </Modal>
  );
};

export default CustomDateModel;
