import React, { memo, useEffect, useRef, useState } from "react";
import "./dashboard.scss";
import DropdownMultiselect from "react-multiselect-dropdown-bootstrap";
import uparrow from "../../assets/images/up-arrow.png";
import downorangearrow from "../../assets/images/down-orange-arrow.png";
import sidearrow from "../../assets/images/side-arrow.png";
import "react-datepicker/dist/react-datepicker.css";
import Lightpick from "lightpick";
import "lightpick/css/lightpick.css";
import moment from "moment";
import { useDispatch } from "react-redux";
import { getData } from "../../redux/dashboard/dashboardAction";
import { useSelector } from "react-redux/es/exports";
import CustomDateModel from "./Modal";
// import Modal from "./Modal";
const Dashboard = ({ setPayloadData }) => {
  const dashboardData = useSelector((state) => state.dashboard);
  var date_picker_dashboard;
  const dispatch = useDispatch();

  // console.log(dashboard);

  // const [select_date_picker, setDatePicker] = useState(null);
  const [multiSelect, setMultiSelect] = useState([]);
  const [days, setDays] = useState("7");
  const [startDate, setStartDate] = useState(
    moment().clone().subtract(7, "days")
  );
  const [endDate, setEnddate] = useState(moment());
  const [duration, setDuration] = useState("This Week");
  const [campaigns, setCampaigns] = useState("Inbound");
  // const [LeadSource, setLeadSource] = useState();

  const dateref = useRef(null);

  const onchangeDropdown = (selected) => {
    setMultiSelect(selected);
  };

  useEffect(() => {
    dateFilter();
    // eslint-disable-next-line
  }, []);
  const payload = {
    lead_source: multiSelect,
    type: campaigns,
    days: days,
  };

  useEffect(() => {
    console.log(
      "useeffect call",
      duration,
      campaigns,
      JSON.stringify(dashboardData),
      days
    );
    dispatch(getData(payload));
    // dispatch(getBoundData(payload));
    setPayloadData(payload);

    return () => console.log("unmount");
    // eslint-disable-next-line
  }, [duration, campaigns, days]);
  // console.log(days, "days");

  const dateFilter = () => {
    // const selected_date = document.getElementById("selected_date");
    if (dateref.current) {
      date_picker_dashboard = new Lightpick({
        field: dateref.current,
        singleDate: false,
        selectForward: true,
        repick: true,
        startDate: startDate,
        endDate: moment(),
        maxDate: moment(),
        minDate: moment(startDate),

        inline: false,
        onSelect: function (start, end) {
          // console.log("start,end");
          var str = "";
          // start ? setStartDate(start) : moment(startDate).format("MMM D YYYY");
          // end ? setEnddate(end) : moment(endDate).format("MMM D YYYY");
          str += start ? start.format("MMM D YYYY") + " - " : "";
          str += end ? end.format("MMM D YYYY") : "...";
          dateref.current.value = str;
          // console.log("str", str);

          // console.log("dateDiff: " + dateDiff);
          // console.log(dateDiff);
          setDays(end.diff(start, "days").toString());
        },
      });
      var str = "";
      str += moment(startDate).format("MMM D YYYY") + " - ";
      str += moment(endDate).format("MMM D YYYY");
      // console.log("str", str);
      // setCustomeDate(str);
      dateref.current.value = str;
      // setDatePicker(date_picker_dashboard);
    }
  };

  const setDateFilter = (duration) => {
    // console.log(duration, "Duration");
    switch (duration) {
      case "This Week":
        // console.log("This week call");
        let start = moment().clone().subtract(7, "days");
        setStartDate(start);
        setEnddate(moment().clone().format("YYYY-MM-DD"));
        date_picker_dashboard?.setDateRange(
          moment().clone().subtract(7, "days"),
          moment()
        );

        date_picker_dashboard?.reloadOptions({
          minDate: moment().clone().subtract(7, "days"),
          maxDate: moment(),
        });

        break;
      case "This Month":
        // console.log(select_date_picker, "select_date_picker");
        setStartDate(moment().startOf("month"));
        setEnddate(moment().clone());
        date_picker_dashboard?.setDateRange(
          moment().startOf("month"),
          moment()
        );
        date_picker_dashboard?.reloadOptions({
          minDate: moment().startOf("month"),
          maxDate: moment(),
        });

        break;
      case "This Year":
        setStartDate(moment().startOf("year"));
        setEnddate(moment());

        date_picker_dashboard?.setDateRange(moment().startOf("year"), moment());
        date_picker_dashboard?.reloadOptions({
          minDate: moment().startOf("year"),
          maxDate: moment(),
        });

        break;
      default:
        break;
    }
  };

  const filterData = (duration) => {
    console.log(duration);
    if (duration === "Custom") {
      handleShow();
    } else {
      setDuration(duration);
      dateFilter();
      setDateFilter(duration);
    }
  };

  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  // console.log("show", show);

  // useEffect(() => {
  //  if(customDate){

  //  }
  // }, [customDate])

  return (
    <>
      <section className="time-filter-data">
        <div className="container">
          <div className="time-wrapper">
            <div className="row">
              <div className="col-lg-4 col-sm-3">
                <div className="form-group">
                  <label>{duration}</label>
                  <input
                    type="text"
                    id="selected_date"
                    className="blue-text"
                    ref={dateref}
                    readOnly
                    // value={customDate}
                  />
                  {/* <p
                    
                    onClick={() =>
                      document.getElementById("selected_date").click()
                    }
                  > */}

                  {/* {{from_date|date:'MMM d y'}} - {{to_date|date:'MMM d y'}} */}
                  {/* </p> */}
                </div>
              </div>
              <div className="col-lg-8 col-md-9 col-sm-12">
                <div className="select-wrapper">
                  <div className="form-group common-select hide-mobile">
                    <label>Lead Source</label>
                    {/* <select id="multiple-dateFilterboxes" multiple="multiple">
                      <option value="Kelley Blue Book">Kelley Blue Book</option>
                      <option value="Buy Your Car">Buy Your Car</option>
                      <option value="Henson Nation">Henson Nation</option>
                      <option value="CarGuru">CarGuru</option>
                      <option value="AutoTrader">AutoTrader</option>
                      <option value="Other">Other</option>
                    </select> */}
                    {dashboardData?.leadSource?.length !== 0 && (
                      <DropdownMultiselect
                        placeholder="select option"
                        // selected={[leadSource[0]]}
                        options={dashboardData?.leadSource}
                        buttonClass="form-select"
                        handleOnChange={onchangeDropdown}
                        // defaultValue={[leadSource[0]]}
                      />
                    )}
                  </div>
                  <div className="responsive-select">
                    <div className="form-group common-select">
                      <label>Campaigns</label>
                      <select
                        className="form-select"
                        aria-label="Default select example"
                        onChange={(e) => setCampaigns(e.target.value)}
                        value={campaigns}
                      >
                        <option defaultValue value="Inbound">
                          Inbound
                        </option>
                        <option value="Outbound">Outbound</option>
                        <option value="Inbound & Outbound">
                          Inbound & Outbound
                        </option>
                      </select>
                    </div>
                    <div className="form-group time-select">
                      <label>Time Period</label>
                      <select
                        className="form-select"
                        aria-label=" Default select example"
                        onChange={(e) => filterData(e.target.value)}
                        value={duration}
                      >
                        <option defaultValue value="This Week">
                          This Week
                        </option>
                        <option value="This Month">This Month</option>
                        <option value="This Year">This Year</option>
                        <option value="Custom">Custom</option>
                      </select>
                    </div>
                  </div>
                  <div className="form-group">
                    <button
                      type="button"
                      className="btn btn-primary common-btn"
                      onClick={() => window.print()}
                    >
                      Download PDF
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="box-detail-main">
            <div className="row">
              <div className="col-md-4 col-sm-12">
                <div className="row">
                  <div className="col-md-10">
                    <div className="row g-3">
                      <div className="col-md-6">
                        <div className="box-inner">
                          <div className="box-div">
                            <p>Text Sent</p>
                            <h3>
                              {
                                dashboardData?.dashboard?.messages?.sent
                                  ?.current
                              }
                            </h3>
                            <div className="d-flex">
                              <p className="green">
                                <img src={uparrow} alt="uparrow" />
                                {
                                  dashboardData?.dashboard?.messages?.sent
                                    ?.change
                                }
                              </p>
                              <span>
                                {
                                  dashboardData?.dashboard?.messages?.sent
                                    ?.complete
                                }{" "}
                                Total
                              </span>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="box-inner">
                          <div className="box-div">
                            <p>Text Received</p>
                            <h3>
                              {
                                dashboardData?.dashboard?.messages?.received
                                  ?.current
                              }
                            </h3>
                            <div className="d-flex">
                              <p className="green">
                                <img src={uparrow} alt="uparrow" />

                                {
                                  dashboardData?.dashboard?.messages?.received
                                    ?.change
                                }
                              </p>
                              <span>
                                {
                                  dashboardData?.dashboard?.messages?.received
                                    ?.complete
                                }{" "}
                                Total
                              </span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-4 col-sm-12">
                <div className="row">
                  <div className="col-md-10 offset-md-1">
                    <div className="row g-3">
                      <div className="col-md-6">
                        <div className="box-inner">
                          <div className="box-div">
                            <p>Engagement</p>
                            <h3>
                              {
                                dashboardData?.dashboard?.messages?.received
                                  ?.current
                              }
                            </h3>
                            <div className="d-flex">
                              <p className="green">
                                <img src={uparrow} alt="uparrow" />
                                {
                                  dashboardData?.dashboard?.messages?.received
                                    ?.change
                                }
                              </p>
                              <span>
                                {
                                  dashboardData?.dashboard?.messages?.received
                                    ?.complete
                                }{" "}
                                Total
                              </span>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="box-inner">
                          <div className="box-div">
                            <p>Appointments</p>
                            <h3>
                              {dashboardData?.dashboard?.appointments?.current}
                            </h3>
                            <div className="d-flex">
                              <p className="green">
                                <img src={uparrow} alt="uparrow" />
                                {dashboardData?.dashboard?.appointments?.change}
                              </p>
                              <span>
                                {
                                  dashboardData?.dashboard?.appointments
                                    ?.complete
                                }{" "}
                                Total
                              </span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-4 col-sm-12">
                <div className="row">
                  <div className="col-md-10 offset-md-2">
                    <div className="row g-3">
                      <div className="col-md-6">
                        <div className="box-inner">
                          <div className="box-div">
                            <p>Negative</p>
                            <h3>
                              {
                                dashboardData?.dashboard?.leads?.negative
                                  ?.current
                              }
                            </h3>
                            <div className="d-flex">
                              <p className="green">
                                <img src={uparrow} alt="uparrow" />
                                {
                                  dashboardData?.dashboard?.leads?.negative
                                    ?.change
                                }
                              </p>
                              <span>
                                {
                                  dashboardData?.dashboard?.leads?.negative
                                    ?.complete
                                }{" "}
                                Total
                              </span>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="box-inner">
                          <div className="box-div">
                            <p>Invalid Leads</p>
                            <h3>
                              {dashboardData?.dashboard?.leads?.bad?.current}
                            </h3>
                            <div className="d-flex">
                              <p className="green">
                                <img src={uparrow} alt="uparrow" />
                                {dashboardData?.dashboard?.leads?.bad?.change}
                              </p>
                              <span>
                                {dashboardData?.dashboard?.leads?.bad?.complete}{" "}
                                Total
                              </span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="col-md-4 col-sm-12">
                <div className="row">
                  <div className="col-md-10">
                    <div className="row g-3">
                      <div className="col-md-6">
                        <div className="box-inner">
                          <div className="box-div">
                            <p>Calls Triggered</p>
                            <h3>
                              {
                                dashboardData?.dashboard?.calls?.triggered
                                  ?.current
                              }
                            </h3>
                            <div className="d-flex">
                              <p className="green">
                                <img src={uparrow} alt="uparrow" />
                                {
                                  dashboardData?.dashboard?.calls?.triggered
                                    ?.change
                                }
                              </p>
                              <span>
                                {
                                  dashboardData?.dashboard?.calls?.triggered
                                    ?.complete
                                }{" "}
                                Total
                              </span>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="box-inner">
                          <div className="box-div">
                            <p>Calls Answered</p>
                            <h3>
                              {
                                dashboardData?.dashboard?.calls?.answered
                                  ?.current
                              }
                            </h3>
                            <div className="d-flex">
                              <p className="green orange">
                                <img
                                  src={downorangearrow}
                                  alt="downorangearrow"
                                />
                                {
                                  dashboardData?.dashboard?.calls?.answered
                                    ?.change
                                }
                              </p>
                              <span>
                                {
                                  dashboardData?.dashboard?.calls?.answered
                                    ?.complete
                                }{" "}
                                Total
                              </span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="col-md-4 col-sm-12">
                <div className="row">
                  <div className="col-md-10 offset-md-1">
                    <div className="row g-3">
                      <div className="col-md-6">
                        <div className="box-inner">
                          <div className="box-div">
                            <p>Positive</p>
                            <h3>
                              {
                                dashboardData?.dashboard?.leads?.positive
                                  ?.current
                              }
                            </h3>
                            <div className="d-flex">
                              <p className="green">
                                <img src={uparrow} alt="uparrow" />
                                {
                                  dashboardData?.dashboard?.leads?.positive
                                    ?.change
                                }
                              </p>
                              <span>
                                {
                                  dashboardData?.dashboard?.leads?.positive
                                    ?.complete
                                }{" "}
                                Total
                              </span>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="box-inner">
                          <div className="box-div">
                            <p>No Response</p>
                            <h3>
                              {
                                dashboardData?.dashboard?.leads?.noresponse
                                  ?.current
                              }
                            </h3>
                            <div className="d-flex">
                              <p className="green blue">
                                <img src={sidearrow} alt="sidearrow" />
                                {
                                  dashboardData?.dashboard?.leads?.noresponse
                                    ?.change
                                }
                              </p>
                              <span>
                                {
                                  dashboardData?.dashboard.leads?.noresponse
                                    ?.complete
                                }{" "}
                                Total
                              </span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="col-md-4 col-sm-12">
                <div className="row">
                  <div className="col-md-10 offset-md-2">
                    <div className="row g-3">
                      <div className="col-md-6">
                        <div className="box-inner">
                          <div className="box-div">
                            <p>Missed Calls</p>
                            <h3>
                              {dashboardData?.dashboard.calls?.missed?.current}
                            </h3>
                            <div className="d-flex">
                              <p className="green orange">
                                <img
                                  src={downorangearrow}
                                  alt="downorangearrow"
                                />
                                {dashboardData?.dashboard.calls?.missed?.change}
                              </p>
                              <span>
                                {
                                  dashboardData?.dashboard.calls?.missed
                                    ?.complete
                                }{" "}
                                Total
                              </span>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="box-inner">
                          <div className="box-div">
                            <p>Opt-Outs</p>
                            <h3>
                              {dashboardData?.dashboard?.leads?.optout?.current}
                            </h3>
                            <div className="d-flex">
                              <p className="green orange">
                                <img
                                  src={downorangearrow}
                                  alt="downorangearrow"
                                />
                                {
                                  dashboardData?.dashboard?.leads?.optout
                                    ?.change
                                }
                              </p>
                              <span>
                                {
                                  dashboardData?.dashboard?.leads?.optout
                                    ?.complete
                                }{" "}
                                Total
                              </span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <CustomDateModel
          handleClose={handleClose}
          handleShow={handleShow}
          setShow={setShow}
          show={show}
          dateref={dateref}
          setDuration={setDuration}
          setDays={setDays}
        />
      </section>
    </>
  );
};

export default memo(Dashboard);
