import React, { useState } from "react";
import "./header.scss";
import logo from "../../assets/images/logo.png";
import user from "../../assets/images/user.png";
import moment from "moment";

const Header = () => {
  const [currentDate, setCurrentDate] = useState(moment().format("LTS"));
  setInterval(() => {
    setCurrentDate(moment().format("LTS"));
  }, 1000);
  return (
    <header>
      <nav className="navbar navbar-expand-lg custom-navbar">
        <div className="container">
          <a className="navbar-brand" href="#navbrand">
            <img src={logo} alt="logo" />
          </a>
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarCollapse"
            aria-controls="navbarCollapse"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icons"></span>
            <span className="navbar-toggler-icons"></span>
            <span className="navbar-toggler-icons"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarCollapse">
            <ul className="navbar-nav me-auto mb-2 mb-md-0">
              <li className="nav-item">
                <a
                  className="nav-link active"
                  aria-current="page"
                  href="#Overview"
                >
                  Overview
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#Lead">
                  Leads
                </a>
              </li>
              <li className="nav-item">
                <a
                  className="nav-link "
                  href="#navlink"
                  tabIndex="-1"
                  aria-disabled="true"
                >
                  Management
                </a>
              </li>
              <li className="nav-item time">
                <p>
                  <span>{currentDate} </span>
                  {moment().format("dddd")}
                  {moment().format("MMMM, DD YYYY")}
                </p>
              </li>
            </ul>

            <ul className="d-flex right-menu">
              <li className="nav-item dropdown custom-dropdown">
                <label>View as</label>
                <a
                  className="nav-link dropdown-toggle"
                  href="#dropdown"
                  id="navbarDropdown"
                  role="button"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                >
                  Ebunch
                </a>
                <ul
                  className="dropdown-menu dropdown-menu-end"
                  aria-labelledby="navbarDropdown"
                >
                  <li>
                    <a className="dropdown-item" href="#ebunch 1">
                      ebunch 1
                    </a>
                  </li>
                  <li>
                    <a className="dropdown-item" href="# ebunch 2">
                      ebunch 2
                    </a>
                  </li>
                  <li>
                    <a className="dropdown-item" href="#ebunch 3">
                      ebunch 3
                    </a>
                  </li>
                </ul>
              </li>
              <li className="nav-item dropdown user-dropdown">
                <a
                  className="nav-link dropdown-toggle"
                  href="#dropdown"
                  id="navbarDropdown"
                  role="button"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                >
                  <img src={user} alt="profile" />
                  Benjamin Combs
                </a>
                <ul
                  className="dropdown-menu dropdown-menu-end"
                  aria-labelledby="navbarDropdown"
                >
                  <li>
                    <a className="dropdown-item" href="#Profile">
                      Profile
                    </a>
                  </li>
                  <li>
                    <a className="dropdown-item" href="#Logout">
                      Logout
                    </a>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </header>
  );
};

export default Header;
