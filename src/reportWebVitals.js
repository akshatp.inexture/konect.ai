const reportWebVitals = onPerfEntry => {
  if (onPerfEntry && onPerfEntry instanceof Function) {
    import('web-vitals').then(({ getCLS, getFID, getFCP, getLCP, getTTFB }) => {
      getCLS(onPerfEntry); //Cumulative Layout Shift (CLS) measures visual stability(<=0.1)
      getFID(onPerfEntry); //First Input Delay (FID) measures interactivity(<=100ms)
      getFCP(onPerfEntry); //First Contentful Paint (FCP) useful in diagnosing issues with LCP 
      getLCP(onPerfEntry); //Largest Contentful Paint (LCP) measures loading performance.
      getTTFB(onPerfEntry); //Time To First Byte (TTFB) useful in diagnosing issues with LCP 
    });
  }
};

export default reportWebVitals;
