import React, { useState } from "react";
import Dashboard from "../components/dashboard/Dashboard";
import Header from "../components/header/Header";
import BoundTable from "../components/boundtable/BoundTable";
import Leadchart from "../components/leadchart/Leadchart";
import Heatmap from "../components/heatmap/Heatmap";
import Wordcloud from "../components/wordcloud/Wordcloud";

const Overview = () => {
  const [payloadData, setPayloadData] = useState(null);
  return (
    <>
      <div className="">
        <div className="">
          <Header />
        </div>
      </div>
      <div className="container">
        <div className="row">
          <Dashboard setPayloadData={setPayloadData} />
        </div>
        <div className="leads-main">
          <div className="container">
            <div className="leads-inner">
              <div className="row">
                <div className="col-md-6 mb-4">
                  <Leadchart />
                </div>
                <div className="col-md-6 mb-4">
                  <Heatmap />
                </div>
              </div>
            </div>
          </div>
        </div>
        <section className="table-main">
          <div className="container">
            <div className="tab-word-cloud-inner">
              <div className="row">
                <div className="col-md-6">
                  <BoundTable payloadData={payloadData} />
                </div>
                <div className="col-md-6">
                  <Wordcloud />
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </>
  );
};

export default Overview;
