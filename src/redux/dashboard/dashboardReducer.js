import { GET_DATA, GET_LEAD_SOURCE, GET_BOUND_DATA } from "./dashboardType";
const initialState = {
  dashboard: {},
  leadSource: [],
  boundData: {},
};

const dashboardReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_DATA: {
      return {
        ...state,
        dashboard: action.payload,
      };
    }
    case GET_LEAD_SOURCE: {
      return {
        ...state,
        leadSource: action.payload,
      };
    }
    case GET_BOUND_DATA: {
      return {
        ...state,
        boundData: action.payload,
      };
    }
    default:
      return state;
  }
};

export default dashboardReducer;
