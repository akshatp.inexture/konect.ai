import axios from "axios";
import { GET_DATA, GET_LEAD_SOURCE, GET_BOUND_DATA } from "./dashboardType";

export const getData = (value) => async (dispatch) => {
  // console.log(value);
  const res = await axios.post(
    "https://devreports.konect.ai/reporting/api/v1/dashboard",
    value,
    {
      headers: {
        "Content-Type": "application/json",
      },
    }
  );
  console.log(res.data.data);
  dispatch({
    type: GET_DATA,
    payload: res.data.data,
  });
};

export const getLeadSource = () => async (dispatch) => {
  try {
    const res = await axios.get(
      "https://devreports.konect.ai/reporting/api/v1/report/lead-source"
    );
    // console.log(res.data.data);
    // setLeadSource(res.data.data);
    dispatch({
      type: GET_LEAD_SOURCE,
      payload: res.data.data,
    });
  } catch (error) {
    console.log(error);
  }
};

export const getBoundData = (value) => async (dispatch) => {
  try {
    const res = await axios.post(
      "https://devreports.konect.ai/reporting/api/v1/campaign",
      value,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    // console.log(res.data.data);
    // setLeadSource(res.data.data);
    dispatch({
      type: GET_BOUND_DATA,
      payload: res.data,
    });
  } catch (error) {
    console.log(error);
  }
};
