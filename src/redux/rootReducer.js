import { combineReducers } from "redux";
import dashboardReducer from "./dashboard/dashboardReducer";

const rootReducer = combineReducers({
  dashboard: dashboardReducer,
  //   user: userReducer,
  //   news: newsReducer,
  //   bookmark: bookmarkReducer,
});

export default rootReducer;
